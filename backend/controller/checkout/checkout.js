const generateOrderDetails = require("../generateOrderDetails");
const getTopPromo = require("../getTopPromo");
const topPromos = require("../../data/topPromos");

const checkOutController = {
  getCheckOutDetails: (req, res) => {
    try {
      const orders = req.query;
      // console.log(orders);

      // * checks if there are query parameters included in the request
      // * checks also if only the promo are included
      if (Object.keys(orders).length === 0) {
        res.status(404).send({ msg: "No details included on the request" });
      } else if (
        Object.keys(orders).length === 1 &&
        Object.keys(orders)[0] === "top"
      ) {
        res.status(404).send({ msg: "No orders found" });
      } else {
        // * processing of the request with validated query params
        const productOrderDetails = {};

        for (let product in orders) {
          // console.log(product);
          // console.log(orders[product]);

          // * checks if the current query param is for the top(t1, or t2, etc)
          // * it must be skipped because it is not for a product
          if (product === "top") continue;

          // * checks if there are product orders based on the query params
          // * then generate the details via the generateOrderDetails method
          // * defined on the controller folder
          // * decided to extract the implementation of the generation of orders
          // * so taht whenever there will be changes on the product details and
          // * product promost it will be easier to update
          if (orders[product] > 0) {
            productOrderDetails[product] = generateOrderDetails(
              product,
              orders[product]
            );
          }
        }

        // console.log("productOrderDetails", productOrderDetails.less, productOrderDetails.free.ticket, productOrderDetails.free.qty);

        // * initializing the check out summary object
        // * that will be be sent as response once completed
        let checkOutSummary = {
          TITLE: "CHECKOUT SUMMARY",
          TOTAL_NUMBER_OF_TICKETS: "",
          UNDISCOUNTED_TOTAL_PRICE: "",
          TOTAL_PRICE_AFTER_PRODUCT_DISCOUNT: "",
          TOTAL_PRICE_AFTER_TOP_DISCOUNT: "",
          FREEBIES: [],
        };

        // * computing for the property values needed in the check out summary object
        let totalNumberOfTickets = 0;
        let totalPriceAfterProdDiscount = 0;
        let totalUndiscountedPrice = 0;
        let totalPriceAfterTopDiscount = 0;

        for (let item in productOrderDetails) {
          totalNumberOfTickets += +productOrderDetails[item].qty;
          totalUndiscountedPrice +=
            +productOrderDetails[item].totalUnDiscountedPrice;
          totalPriceAfterProdDiscount +=
            +productOrderDetails[item].totalPriceAfterProdDiscount;
          if (productOrderDetails[item].free.qty > 0) {
            checkOutSummary.FREEBIES.push(productOrderDetails[item].free);
          }
        }

        // * applying the on top promo selected
        // * decided to extract the implementation of the application of
        // * the on top promos
        // * placed the implemenation on the controller folder
        // * so that whenever there will be additional promos or changes in promo
        // * it will be easier to u[date
        // console.log(getTopPromo(orders.top));

        const validCodeFromTopPromos = topPromos.map((e) => e.id);
        // console.log(validCodeFromTopPromos);
        if (orders.top && validCodeFromTopPromos.includes(orders.top)) {
          const topPromo = getTopPromo(orders.top);
          totalPriceAfterTopDiscount =
            totalPriceAfterProdDiscount *
            (1 - topPromo.percentDiscountOnTotal / 100);
          if (topPromo.free.qty > 0) {
            checkOutSummary.FREEBIES.push(topPromo.free);
          }
        } else {
          totalPriceAfterTopDiscount = totalPriceAfterProdDiscount;
        }

        checkOutSummary = {
          ...checkOutSummary,
          TOTAL_NUMBER_OF_TICKETS: totalNumberOfTickets,
          UNDISCOUNTED_TOTAL_PRICE: totalUndiscountedPrice,
          TOTAL_PRICE_AFTER_PRODUCT_DISCOUNT: totalPriceAfterProdDiscount,
          TOTAL_PRICE_AFTER_TOP_DISCOUNT: totalPriceAfterTopDiscount,
        };

        // res.status(200).json(productOrderDetails);
        res.status(200).json(checkOutSummary);
      }
    } catch (error) {
      console.error(error);
      res
        .status(500)
        .json({ errorMsg: `An error occured while generating the request.` });
    }
  },
};

module.exports = checkOutController;
