const topPromos = require("../data/topPromos");

// determines what top promo to be included based on the code in the query params
const getTopPromo = (code) => {
  const promo = topPromos.filter((e) => e.id === code)[0];
  // console.log(promo);
  return {
    percentDiscountOnTotal: promo.percentDiscountOnTotal,
    free: promo.free,
  };
};

module.exports = getTopPromo;
