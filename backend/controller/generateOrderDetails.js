const products = require("../data/products");

// * this generate the order details for products
// * ordered based on the query params send on the request
const generateOrderDetails = (prod, qty) => {
  // console.log(prod, qty);
  const product = products.filter((e) => e.id === prod)[0];
  // console.log(product.price);

  // * initializing the stucture of the object to be returned
  let productOrderDetails = {
    code: product.id,
    name: product.name,
    price: product.price,
    qty: qty,
    totalUnDiscountedPrice: qty * product.price,
    less: "",
    free: "",
    totalPriceAfterProdDiscount: qty * product.price,
  };

  // * included so that the request will accept even though some quantities are zero
  // * but the product with zero amount on the query params will not be included on the response
  if (qty < product.promo.minPurchase) return productOrderDetails;

  // * calculating the discounts and freebies to be included
  // * based on the current settings of the individual product promos
  // * as defined on the data/products.js
  let less = +(product.promo.percentDiscountOnSelf / 100) * product.price;
  let multiplierForLess = Math.floor(qty / product.promo.multiples);
  let multiplierForFree = Math.floor(qty / product.promo.multiples);

  // console.log("COMP", multiplierForLess, multiplierForFree);
  const max = +product.promo.maxCount;
  let nLess = multiplierForLess > max ? max : multiplierForLess;
  let nFree = multiplierForFree > max ? max : multiplierForFree;
  // console.log("COMP2", nLess, nFree, max);
  let finalValueForLess = +(less * nLess);
  let tpAfterProdDiscount = +(
    productOrderDetails.totalUnDiscountedPrice - finalValueForLess
  );
  // * setting the final values of the object to be returned
  productOrderDetails = {
    ...productOrderDetails,
    less: finalValueForLess,
    free: {
      ticket: product.promo.free,
      qty: `${product.promo.free ? nFree : ""}`,
    },
    totalPriceAfterProdDiscount: tpAfterProdDiscount,
  };

  return productOrderDetails;
};

module.exports = generateOrderDetails;
