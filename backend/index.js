const checkoutRoute = require("./routes/checkout");
const express = require("express");

const app = express();
const PORT = 3001;

app.use(express.json());
app.use(express.urlencoded({ extended: true }));

// *test endpoint: GET |
// *sample test urls:
// localhost:3001/compute?prod1=6&prod2=5&prod3=2&prod4=2&top=t1
// localhost:3001/compute?prod1=6&prod2=5&prod3=2&prod4=2&top=t2
// localhost:3001/compute?prod1=1&prod2=5&prod3=2&prod4=2&top=t1
// localhost:3001/compute?prod4=2&top=t2&prod1=3&prod3=6
// localhost:3001/compute?prod1=6&prod4=2&top=t2
// localhost:3001/compute?prod4=2&top=t2
// localhost:3001/compute?
// localhost:3001/compute?top=t2
// localhost:3001/compute?prod1=6&prod2=5&prod3=2&prod4=NOTANUMBER&top=t2
// localhost:3001/compute?prod1=6&prod2=5&prod3=2&prod4=2&top=g2
// localhost:3001/home

app.use("/compute?", checkoutRoute);

// for any other routes
app.get("/*", (req, res) => {
  res.status(200).send("....can be some other pages.");
});

app.listen(PORT, () => console.log(`running express on port ${PORT}`));
