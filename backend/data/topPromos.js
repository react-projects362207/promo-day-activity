const topPromos = [
  {
    id: "t1",
    description: "FREETWOWAY - adds a round trip ticket",
    free: {
      ticket: "Bonus Round Trip Ticket",
      qty: 1,
    },
    percentDiscountOnTotal: 0,
  },
  {
    id: "t2",
    description: "5OFF - gives 5% discount on the total amount",
    free: {
      ticket: "",
      qty: "",
    },
    percentDiscountOnTotal: 5,
  },
];

module.exports = topPromos;
