const products = [
  {
    id: "prod1",
    name: "One-way ticket",
    price: 5000,
    promo: {
      description:
        "Buy 3 one-way ticket, customer receives one free one-way ticket",
      free: "One-way ticket",
      percentDiscountOnTotal: 0,
      percentDiscountOnSelf: 0,
      minPurchase: 3,
      maxCount: Infinity,
      multiples: 3,
    },
  },
  {
    id: "prod2",
    name: "Round trip ticket",
    price: 10000,
    promo: {
      description:
        "Round Trip ticket gets 30% off for every purchase (only up to 5)",
      free: "",
      percentDiscount: 0,
      percentDiscountOnSelf: 30,
      minPurchase: 1,
      maxCount: 5,
      multiples: 1,
    },
  },
  {
    id: "prod3",
    name: "Half a year unli travel subscription",
    price: 50000,
    promo: {
      description:
        "Half a year unli travel subscription gets 10% if two are bought at once",
      free: "",
      percentDiscount: 0,
      percentDiscountOnSelf: 20, //change this to 10 if what is meant by 10% off is for the two
      minPurchase: 2,
      maxCount: Infinity,
      multiples: 2,
    },
  },
  {
    id: "prod4",
    name: "Full year unli travel subscription",
    price: 100000,
    promo: {
      description: "Full year unli travel subscription is 10% off",
      free: "",
      percentDiscount: 0,
      percentDiscountOnSelf: 10,
      minPurchase: 1,
      maxCount: Infinity,
      multiples: 1,
    },
  },
];

module.exports = products;
