const generateOrderDetails = require("../controller/generateOrderDetails");
const getTopPromo = require("../controller/getTopPromo");
const checkOutController = require("../controller/checkout/checkout");
const express = require("express");
const router = express.Router();

// *test endpoint: GET |
// *sample test urls:
// localhost:3001/compute?prod1=6&prod2=5&prod3=2&prod4=2&top=t1
// localhost:3001/compute?prod1=6&prod2=5&prod3=2&prod4=2&top=t2
// localhost:3001/compute?prod1=1&prod2=5&prod3=2&prod4=2&top=t1
// localhost:3001/compute?prod4=2&top=t2&prod1=3&prod3=6
// localhost:3001/compute?prod1=6&prod4=2&top=t2
// localhost:3001/compute?prod4=2&top=t2
// localhost:3001/compute?
// localhost:3001/compute?top=t2
// localhost:3001/compute?prod1=6&prod2=5&prod3=2&prod4=NOTANUMBER&top=t2
// localhost:3001/compute?prod1=6&prod2=5&prod3=2&prod4=2&top=g2
// localhost:3001/home

// for generating the checkout summary
router.get("/", checkOutController.getCheckOutDetails);

module.exports = router;
